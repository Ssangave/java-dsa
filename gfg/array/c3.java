/* Given an array Arr[] of N integers. Find the contiguous sub-array(containing at least one number) which has the maximum sum and return its sum. 
nput:
N = 4
Arr[] = {-1,-2,-3,-4}
Output:
-1  */

import java.io.*;
class Demo{
        public static void main(String[]a)throws IOException{
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("enter the size");
                int size = Integer.parseInt(br.readLine());
                int arr[]=new int[size];
                System.out.println("arr element");
                for(int i=0;i<size;i++){
                        arr[i] = Integer.parseInt(br.readLine());
                }
		int sum=0,max=arr[0];
		for(int i=0;i<size;i++){
			int sum1=0;
			for(int j=0;j<=i;j++){
				sum1=sum1+arr[j];
				if(max<arr[j])
					max=arr[j];
			}
			if(sum<sum1){
				sum=sum1;
			}			
		}	
		if(max<0)
			System.out.println(max);		
		else
			System.out.println(sum);		
	}
}

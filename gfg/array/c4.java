/*  Given an array of size N containing only 0s, 1s, and 2s; sort the array in ascending order.
Input: 
N = 5
arr[]= {0 2 1 2 0}
Output:
0 0 1 2 2  */
import java.io.*;
class ArrayDemo{
	public static void main(String[]a)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("size");
		int size = Integer.parseInt(br.readLine());
		int arr[] = new int[size];
		for(int i=0;i<arr.length;i++){
			arr[i] = Integer.parseInt(br.readLine());
		}
		
		for(int i=0;i<size-1;i++){
			int max=arr[0],k=0;
			for(int j=0;j<size-i;j++){
				if(max<arr[j]){
					max=arr[j];
					k=j;
				}
			}
			arr[k]=arr[size-1-i];
			arr[size-i-1]=max;	
		}
		for(int i=0;i<arr.length;i++){
			System.out.println(arr[i]);
		}
	}
}

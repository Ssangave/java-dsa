/*  Given an array A of positive integers. Your task is to find the leaders in the array. An element of array is leader if it is greater than or equal to all the elements to its right side. The rightmost element is always a leader. 
Input:
n = 6
A[] = {16,17,4,3,5,2}
Output: 17 5 2         */

import java.io.*;
class ArrayDemo{
        public static void main(String[]a)throws IOException{
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("size");
                int size = Integer.parseInt(br.readLine());
                int arr[] = new int[size];
                for(int i=0;i<arr.length;i++){
                        arr[i] = Integer.parseInt(br.readLine());
                }

                for(int i=0;i<size;i++){
                        int max=arr[i],flag=0;
                        for(int j=i;j<size;j++){
				if(max<arr[j]){
					flag=1;
					break;
				}
			}
			if(flag==0)
				System.out.println(arr[i]);
		}
	}
}

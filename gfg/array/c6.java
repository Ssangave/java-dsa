// k-th smallest no in array
import java.io.*;
class ArrayDemo{
        public static void main(String[]a)throws IOException{
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("size");
                int size = Integer.parseInt(br.readLine());
                int arr[] = new int[size];
                for(int i=0;i<arr.length;i++){
                        arr[i] = Integer.parseInt(br.readLine());
                }
		int min=arr[0];
		for(int i=0;i<arr.length;i++){
			if(min>arr[i])
				min=arr[i];
		}
		for(int i=0;i<arr.length;i++){
			int count=0;
			for(int j=0;j<arr.length;j++){
				if(arr[i]>arr[j]){
					count++;	
				}
			}
			if(count==min-1){
				System.out.println(arr[i]);	
				break;
			}
		}
	}
}

/*     Given an unsorted array A of size N that contains only positive integers, find a continuous sub-array that adds to a given number S and return the left and right index(1-based indexing) of that subarray.											
In case of multiple subarrays, return the subarray indexes which come first on moving from left to right. */

import java.io.*;
class Demo{
	public static void main(String[]a)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("enter the size");
		int size = Integer.parseInt(br.readLine());
		int arr[]=new int[size];
		System.out.println("arr element");
		for(int i=0;i<arr.length;i++){
			arr[i] = Integer.parseInt(br.readLine());
		}
		int temp=12,flag=0,count1=0;
		if(size>1){
			for(int i=0;i<arr.length;i++){
				int sum=0,count=0;
				for(int j=i;j<arr.length;j++){
					sum=sum+arr[j];
					if(sum==temp){
						count=j+1;
						break;
					}
				}
				flag=i+1;
				count1=count;
				if(sum==temp)
					break;
			}
			System.out.println("starting="+flag+" ending="+count1);
		}else
			System.out.println("size 1 or less than 1");
	}
}

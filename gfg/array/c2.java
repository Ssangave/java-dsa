/*  Given an array of size N-1 such that it only contains distinct integers in the range of 1 to N. Find the missing element.  */

import java.io.*;
class Demo{
        public static void main(String[]a)throws IOException{
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("enter the size");
                int size = Integer.parseInt(br.readLine());
                int arr[]=new int[size];
                System.out.println("arr element");
                for(int i=0;i<size-1;i++){
                        arr[i] = Integer.parseInt(br.readLine());
                }
	
		for(int i=1;i<=size;i++){
			int flag=0;
			for(int j=0;j<size-1;j++){
				if(i!=arr[j])
					flag=1;
				else{
					flag=0;
					break;
				}
			}
			if(flag==1){
				System.out.println("diff element is = "+i);
				break;
			}
		}
	}
}

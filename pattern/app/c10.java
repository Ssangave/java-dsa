/* 1 1 2 1 3 1
   1 2 2 2 3 2
   1 3 2 3 3 3
   1 2 2 4 3 4
   1 5 2 5 3 5
*/
class Pattern{
	public static void main(String[]a){
		int row=5;
		for(int i=1;i<=row;i++){
			int temp=1;
			for(int j=1;j<=row+1;j++){
				if(j%2==0)
					System.out.print(i+" ");
				else
					System.out.print(temp+++" ");
			}
			System.out.println();
		}
	}
}

class Pattern{
	public static void main(String[]s){
		int row=5,a=1,b=9;
		for(int i=row;i>=1;i--){
			int num=i;
			for(int j=1;j<=row;j++){
				if(j%2==1){
					System.out.print(num+" ");
					num=num+a;
				}else{
					System.out.print(num+" ");	
					num=num+b;
				}
			}	
			a=a+2;
			b=b-2;
			System.out.println();	
		}
	}
}

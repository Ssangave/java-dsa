class Pattern{
	public static void main(String[]s){
		int row=5;

		for(int i=row;i>=1;i--){
			int num=i;
			for(int j=1;j<=row;j++){
				System.out.print(num+" ");
				num=num+row;
			}
			System.out.println();
		}
	}
}

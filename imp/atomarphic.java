// 25-625
class Automarphic{
	public static void main(String[]args){
		int N=5;
		int temp1=N;
		int square=N*N;
		int count=0;

		int num1 = N;
		int num2 = N*N;			
	
		while(temp1!=0){
			count++;
			temp1=temp1/10;
		}

		int flag=0;
		
		while(count!=0){
			int rem1=square%10;
			int rem2=N%10;

			if(rem1 == rem2){
				flag =  1;
			}else{
				flag = 0;
			}
			square = square / 10;
			N = N / 10;
			count--;
		}

		if(flag == 1)
			System.out.println("A");
		else
			System.out.println("N");
	}
}

import java.io.*;
import java.util.*;
class Demo{
	public static void main(String[]s)throws IOException{
		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the row:");
		int row = Integer.parseInt(br.readLine());
		int num = row;

		for(int i=1;i<=row;i++){
			int num1=num*i;
			for(int j=1;j<=row-i+1;j++){
				System.out.print(num1+" ");
				num1=num1-i;
			}
			num--;
			System.out.println();
		}
	}
}

// D4 C3 B2 A1
//   A1 B2 C3 D4
//   D1 C3 B2 A1
//   A1 B2 C3 D4
//
import java.io.*;
import java.util.*;
class Demo{
        public static void main(String[]args)throws IOException{
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        	System.out.println("Enter the row:");
                int row= Integer.parseInt(br.readLine());

		System.out.println(row);			
		char ch=(char)(64+row);	
		int num =row;
		for(int i=1;i<=row;i++){
			for(int j=1;j<=row;j++){
				if(i%2!=0){
					System.out.print(ch-- +"" +num--+" ");
				}else						
					System.out.print(++ch+""+ ++num +" ");
			}
			System.out.println();
		}
        }
}


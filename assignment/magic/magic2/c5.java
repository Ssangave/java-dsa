import java.io.*;

class Demo{
	public static void main(String[]a)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
											
		int row = Integer.parseInt(br.readLine());
		int i=0,j=1,sum=0;
	
		for(int c=1;c<=row;c++){
			for(int x=1;x<=c;x++){
				System.out.print(sum+" ");
				i=j;
				j=sum;
				sum=i+j;
			}
			System.out.println();
		}
	}
}

class D{
	public static void main(String[]s){
		int row=4;
		int ch=64+row*(row+1)/2;
		for(int i=1;i<=row;i++){
			for(int j=1;j<=i;j++){
				System.out.print((char)ch--+" ");
			}
			System.out.println();
		}
	}
}

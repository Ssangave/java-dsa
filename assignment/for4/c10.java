class D{
	public static void main(String[]s){
		int row=4;	
		for(int i=1;i<=row;i++){
			int num=i;
			for(int j=1;j<=i;j++){
				if((i+j)%2==1)					
					System.out.print(num*num*num+" ");
				else
					System.out.print(num*num+" ");
				num++;
			}
			num--;
			System.out.println();
		}
	}
}

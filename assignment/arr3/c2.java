// reverse no.
import java.io.*;
class DemoComposite{
        public static void main(String[]a)throws IOException{
                BufferedReader br =new BufferedReader(new InputStreamReader(System.in));
                int size = Integer.parseInt(br.readLine());                                                                                            
                int arr[] = new int[size];
                for(int i=0;i<arr.length;i++){
                        arr[i] = Integer.parseInt(br.readLine());
                }
			
		for(int i=0;i<arr.length;i++){
			int rev=0,num=arr[i],rem=0;
			while(num!=0){
				rem=num%10;
				rev=rev*10+rem;
				num=num/10;
			}
			System.out.println(rev);
		}
	}
}		

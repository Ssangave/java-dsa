// strong no.

import java.io.*;
class DemoComposite{
        public static void main(String[]a)throws IOException{
                BufferedReader br =new BufferedReader(new InputStreamReader(System.in));
                int size = Integer.parseInt(br.readLine());                                                                                            
                int arr[] = new int[size];
                for(int i=0;i<arr.length;i++){
                        arr[i] = Integer.parseInt(br.readLine());
                }
		for(int i=0;i<arr.length;i++){
			int num=arr[i];
			int sum = 0;
			while(num!=0){
				int rem=num%10;
				int product = 1;

				for(int j = 1 ; j <= rem ; j++){
					product = product * j;
				}
				sum = sum + product;
				num=num/10;
			}
			if(sum == arr[i]){
				System.out.println("Strong no = " +arr[i]);
			}
		}
	}
}


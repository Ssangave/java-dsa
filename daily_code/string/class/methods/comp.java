// compare tow string 

import java.io.*;
class StringDemo{
	static int mystrcomp(char arr1[],char arr2[]){
		int diff=0;
		for(int i=0;i<arr1.length;i++){
			if(arr1[i]!=arr2[i]){
				diff = arr1[i] - arr2[i];
				return diff;				
			}
		}
		return 0;
	}
        public static void main(String[]a)throws IOException{
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter the string");
                String str1 = br.readLine();
                String str2 = br.readLine();
			
		char arr1[] = str1.toCharArray();
		char arr2[] = str2.toCharArray();
		
		if(arr1.length==arr2.length){
			int str1len=mystrcomp(arr1,arr2);
			if(str1len==0)
				System.out.println("equal");
			else			
				System.out.println("Not equal nd diff = "+ str1len);
		}else
			System.out.println("Not equal");
	}
}

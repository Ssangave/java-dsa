
import java.io.*;
Class StringDemo{
        public static void main(String[]a)throws IOException{
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
	/* >>>  using predefined function <<<<

		String str1 = "shubham";
		String str2= "sangave";
		String str3 = str1.concat(str2);
		System.out.println(str3);
	*/
	// using my function



		String str1 = "shubham";
		String str2 = "sangave";
		char arr1[]  = str1.toCharArray();
		char arr2[]  = str2.toCharArray();
																	
		char arr3[20] = new char[arr1.length + arr2.length];
		for(int i=0;i<arr3.length;i++){
			if(i<arr1.length)
				arr3[i] = arr1[i];
			else
				arr3[i] = arr2[i];
		}
		System.out.println(arr3);
	}
}

class StringDemo{
	public static void main(String[]a){
		String str1 = "shubham";
		String str2 = str1;
		String str3 = new String(str1);
	/*						
		System.out.println(str1);	// scp var jaga milte str1 la
		System.out.println(str2);	// scp
		System.out.println(str3);	// heap var jaga milte
	

		String str4 = "sangave";
		String str5 = str1+str4;
		String str6 = "shubhamsangave";
		String str7 = str1.concat(str4);
		System.out.println(str5);
		System.out.println(str6[0]);				
	
		System.out.println(System.identityHashCode(str5));	// different identityHashCod
		System.out.println(System.identityHashCode(str6));	// different identityHashCod
													
		System.out.println(System.identityHashCode(str7));	// different identityHashCod
		System.out.println(str7);	// different identityHashCod
	
		// >>>> length of string <<<<
		int count=0;
		String str = "shubhamsangave";
		for(int i=0;i<str.length();i++){
			count++;
		}
		System.out.println(count);
		*/
		String str = "shubhamsangave";
		for(int i = 0; i < str.length(); i++)  
		{			  
		// printing in reverse order  
		System.out.print(str.charAt(str.length() - i - 1));  
		}  
	}
}

import java.io.*;
class StringDemo{
	public static void main(String[]s){
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String str1 = "shubham";   				// scp
		String str2 = new String("cor2web");			// heap
		char str3[] = {'s','a','n','g','a','v','e'};		// integer cache
		
		System.out.println("scp var jaga milte >>> "+str1);
		System.out.println(str2);
		System.out.println(str3);
	}
}

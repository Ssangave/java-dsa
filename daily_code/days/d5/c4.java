import java.io.*;
import java.util.*;
class Demo{
	public static void main(String[]s)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		String str = br.readLine();

		System.out.println(str);

		StringTokenizer st = new StringTokenizer(str," ");

		String token1 = st.nextToken();
		String token2 = st.nextToken();
		String token3 = st.nextToken();

		System.out.println(token1);
		System.out.println(token2);
		System.out.println(token3);
	}
}

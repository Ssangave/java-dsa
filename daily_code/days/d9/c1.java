
class Demo{
	static{
		System.out.println("In static 1");	
	}
	
	public static void main(String[]args){
		System.out.println("");
	}
}

class ClientDemo{
	static{
		System.out.println("In static 2");
	}
	public static void main(String[] args){
		System.out.println("In main");
		Demo obj = new Demo();	
	}
	  static{
                System.out.println("In static 3");
        }

}

// method
import java.io.*;
class Demo{
	int fun(int a){
		System.out.println("in fun method...");
		System.out.println(a);
		a++;
		return a;
	}
	public static void main(String[]q)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int a = Integer.parseInt(br.readLine());
									
		System.out.println("in main method...");
		Demo obj = new Demo();
		int ret = obj.fun(a);
		System.out.println(a);
		System.out.println("end main method...");
	}
}

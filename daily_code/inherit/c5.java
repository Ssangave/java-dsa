class parent {
	parent(){
		System.out.println("in constructor p");
	}
	void m1(){					
		System.out.println("in m1");
	}
}
class child extends parent{
	child(){					
		System.out.println("in constructor c");
	}
	void m2(){
		System.out.println("in m2");
	}
}
class Demo{
	public static void main(String[]a){
		parent p1 = new parent();		//<<< constructor call 
		p1.m1();
	//	parent p1 = new child();		//<<< parent and chuld constructor call 
	//	p1.m2();
	}
}

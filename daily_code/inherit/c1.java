class parent {
	int x = 10;
	static int y = 30;
}
class child extends parent{
	int z = 20;
	static int a = 40;
	void m1(){
		System.out.println("in m1");
		System.out.println(z);
		System.out.println(a);
		System.out.println(y);
	}
}
class mainDemo{
	public static void main(String[]a){
		child obj = new child();
		obj.m1();		
		System.out.println(obj.z);
		System.out.println(obj.a);
	}
}

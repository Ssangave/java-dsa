class parent {
	int x = 10;
	static int y = 20;
	parent(){
		System.out.println("in parent constructor");
	}
	static {
		System.out.println("in parent static block");
	}
}
class child1 extends parent{
	static int a = 30;
	int b = 40;
	child1(){
		System.out.println("in child1 constructor");
	}
	static {
		System.out.println("in child1 static constructor");
	}
}
class child2 extends parent{
	static int p = 50;
	int q = 60;
	child2(){
		System.out.println("in child2 constructor");
	}
	static {		
		System.out.println("in child2 static block");
	}
}
class Demo{
	public static void main(String[]args){
		child1 obj = new child1();
		child2 obj1 = new child2();
	}
}

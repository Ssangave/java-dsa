// composition 
class parent {
	int x = 10;
	static int y = 20;

	void m1(){
		System.out.println("in m1");
	}
	static void m2(){
		
		System.out.println("in m2");
	}
}
class child{
	parent p = new parent();		// creating parent object 
}
class Demo{
	public static void main(String[]a){
		child c = new child();
		c.p.m1();
		c.p.m2(); 		// static method axes hote 
//		c.p.x;
	}
}

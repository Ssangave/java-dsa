class parent {
	static int x = 10;
	static {
		System.out.println("static block in p");
	}
	static void access(){
		System.out.println(x);
	}
}
class child extends parent{
	static {
		System.out.println("static block in c");
		System.out.println(x);
		access();
	}
}
class client{
	public static void main(String[]a){
		System.out.println("in main");
		child obj = new child();
	}
}

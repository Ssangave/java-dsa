// constructor 
class parent {
	int x = 10;
	static int y = 20;
	parent(){
		System.out.println("in parent constructor");
	}
	static {
		System.out.println("int parent static block");
	}
}
class child extends parent {
 	child(){
		System.out.println("in child constructor");
		System.out.println(this);
	}
	static {
		System.out.println("in child static block");
	}
}
class Demo{
	public static void main(String[]args){
		child c = new child();
	//	obj.child();
	//	c.y;
	}
}

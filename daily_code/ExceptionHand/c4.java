// userdefined Exception using throw 
import java.util.Scanner;
class DataOverFlowException extends RuntimeException{
	DataOverFlowException(String msg){
		super(msg);
	}
}
class DataUnderFlowException extends RuntimeException{
	DataUnderFlowException(String msg1){
		super(msg1);
		
	}
}
class ArrayDemo{
	public static void main(String[]args){
		int arr[] = new int[5];
		Scanner sc = new Scanner(System.in);
		System.out.println("enter the integer valu");
		System.out.println("note: 0<element<100");
		for(int i=0;i<arr.length;i++){
			int data = sc.nextInt();
			if(data<=0){
				throw new DataUnderFlowException("data less than 0");		// function call with string parameter
			}								
			if(data>100){
				throw new DataOverFlowException("data mare than 100");
			}
		}
	}
}

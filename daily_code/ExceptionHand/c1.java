// Arithmetic Exception

import java.io.*;
class JavaExceptionExample{
	public static void main(String[]args)throws IOException{				
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String str = br.readLine();
		try{
			int data1 = 100/0;
		}			
		catch(ArithmeticException obj){
			System.out.println(obj);
			System.out.println("Exception occured");
		}
	}
}

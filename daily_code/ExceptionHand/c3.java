// 

import java.util.Scanner;
class Demo{
	public static void main(String[]args){
		System.out.println("enter no.");
		Scanner sc = new Scanner(System.in);
		int x = sc.nextInt();
		System.out.println(x);

		try{
			if(x==0){
				throw new ArithmeticException("Divide by Zero");	// using thorw keyword 
			}								
			System.out.println(10/x);
		}	
		catch(ArithmeticException ae){
			System.out.println("Exception in threads"+Thread.currentThread().getName()+" ");
			ae.printStackTrace();
		}
	}
}

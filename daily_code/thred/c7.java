// priority of thread 
class MyThread extends Thread{
	public void run(){
		Thread t = Thread.currentThread();
		System.out.println(t.getPriority());
	}
}
class ThreadDemo{
	public static void main(String[]q){
		Thread t = Thread.currentThread();	
		t.setName("main");
		t.setPriority(9);
		System.out.println("1 - "+t.getPriority());
		System.out.println(t.getName());

		MyThread obj = new MyThread();
		obj.start();

		System.out.println("2 - "+t.getPriority());
		Thread t1 = Thread.currentThread();
		t1.setPriority(7);

		System.out.println("3 - "+t.getPriority());
		MyThread obj1 = new MyThread();
		obj1.start();
		
		System.out.println("4 - "+t.getPriority());
	}
}

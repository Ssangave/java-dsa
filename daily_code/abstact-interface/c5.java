// multi inheritance 

interface Demo1{
	void fun();
	void gun();
}
interface Demo2{
	void fun();
}
class Demochild implements Demo1,Demo2{
	public void fun(){
		System.out.println("in fun");
	}
	public void gun(){
			
	}
}
class client{
	public static void main(String[]a){
		Demo1 obj = new Demochild();
		obj.fun();
		obj.gun();
	}
}

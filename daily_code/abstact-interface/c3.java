// interface
interface Demo{
	void fun();
	void gun();
}
class Demochild implements Demo{
	public  void fun(){			// <<< without public specifire get error
		System.out.println("in fun");
	}					
	public void gun(){
		System.out.println("in gun");
	}	
}
class client {
	public static void main(String[]s){
		Demo obj = new Demochild();
		obj.fun();
	}
}

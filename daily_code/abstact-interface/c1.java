// abstraction 

abstract class parent{
	void career(){
		System.out.println("Doctor");
	}
	abstract void marry();
}
class child extends parent{
	void marry(){
		System.out.println("sm");
	}
}
class Demo{
	public static void main(String[]q){
		parent obj = new child();
		obj.marry();
		obj.career();	
	//	parent obj1 = new parent();	// <<< can't create object of abstrct method
	//	obj1.career();
	}
}

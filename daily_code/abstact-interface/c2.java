// singleton design pattern

class singleton{
	static singleton obj = new singleton();
	private singleton(){
		System.out.println("in constructor");
	}
	void marry(){
		System.out.println("in marry");
	}
	static singleton getobject(){
		return obj;
	}
}
class client{
	public static void main(String[]q){
		singleton obj1 = singleton.getobject();		//<<< only constructor call.....
		System.out.println(obj1);

		singleton obj2 = singleton.getobject();		//<<< only constructor call.....
		System.out.println(obj2);
	
	//	singleton obj1 = singleton.getobject();		//<<< only constructor call.....
	//	System.out.println(obj1);
		
	}					
}

class parent {
	private void fun(){
		System.out.println("parent fun");
	}
	class child extends parent{
		void fun(){
			System.out.println("child fun");
		}
	}
}
class client{
	public static void main(String[]a){
		parent obj = new child();	
		obj.fun();		// <<< Error : fun is private in parent 
	}
}

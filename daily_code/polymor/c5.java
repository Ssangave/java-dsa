class Demo{
	void fun(String str){
		System.out.println("String");
		System.out.println(str);
	}
	void fun(Object str1){			// <<< instead of StringBuffer we can use object
		System.out.println("StringBuffer");
		System.out.println(str1);
	}
}
class client{
	public static void main(String[]a){
		Demo obj = new Demo();
		obj.fun("shubham");
		obj.fun(new StringBuffer("sangave"));
	//	obj.fun(null);
	}
}

class parent{
	void fun(){
		System.out.println("in parent fun");
	}
}
class child extends parent{
	void fun(int x){
		System.out.println("in child fun");
	}
}
class Demo{
	public static void main(String[]s){
		child obj = new child();
		obj.fun();
	}
}

// overloading
class Demo{
	void fun(int x){
		System.out.println(x);
	}
	void fun(float y){
		y++;
		System.out.println(y);
	}
	void fun(Demo obj){
		System.out.println(obj);
		
	}
	public static void main(String[]q){
		Demo obj = new Demo();
		obj.fun(3);
		obj.fun(3.3f);
	
		Demo obj1 = new Demo();
		obj1.fun(obj);
	}
} 


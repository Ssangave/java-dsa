// 2D array

import java.io.*;
class MulDemo{
	public static void main(String[]args){
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		/*							
		int arr[][] = new int[3][3];    // >>>> give compalsary col otherwise nullpointer exception may occurs.  <<<<
		for(int i=0;i<arr.length;i++){
			for(int j=0;j<arr[i].length;j++){
				System.out.println(arr[i][j]);   // output is 9-times >>>>0
			}
		} */
					
		int arr1[][] = new int[3][3];
		int arr2[] = new int[2];
									
		System.out.println(arr1.length);   // >>3
		System.out.println(arr2.length);   // >>2

						
		System.out.println(arr1[1][1]);   // >>2
		System.out.println(arr1[1]);   // >>> address [I@3422
		System.out.println(arr1);   // 	  >>> address [[I@wr342	
	
	}
}


// jagged array memory less consumed comparitivaly normal array 

import java.io.*;
class MulDemo{
        public static void main(String[]args){
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
/*  >>> part-1		jagged array initialisation part-1

		int arr[][] = {{10,20,30},{40,50},{60}};
		for(int i=0;i<arr.length;i++){
			for(int j=0;j<arr[i].length;j++){
				System.out.print(arr[i][j]+" ");
			}
			System.out.println();
		}	*/

// >>> part-2		 jagged array initialisation part-2 

		//int arr[][] = {{10,20,30},{40,50},{60}};
		int arr[][] = new int[3][];
		arr[0] = new int[]{1,2,3};
		arr[1] = new int[]{4,5};
		arr[2] =new int[]{6};
		for(int i=0;i<arr.length;i++){
                       for(int j=0;j<arr[i].length;j++){
                               System.out.print(arr[i][j]+" ");
                       }
                       System.out.println();
                }       

	}
}

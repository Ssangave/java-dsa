//  
import java.io.*;
class Demo{
	public static void main(String[]a)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int size = Integer.parseInt(br.readLine());

		int arr[]=new int[size];		
		System.out.println("Enter array elements:");
		for(int i=0;i<size;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		System.out.println("array elements are:");
		for(int i=0;i<size;i++){
			System.out.println(arr[i]);
		}
	}
}

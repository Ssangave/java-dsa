class StaticDemo{
	static int x = 10;
	static int y = 20;
	static int Disp(){
		System.out.println(x);
		return x;
	}
}
class Client{
	public static void main(String[]args){
		System.out.println(StaticDemo.Disp());							
		StaticDemo.Disp();
	}
}

class Demo{
	static int y=20;
	int x = 10;
	static void fun1(){
		System.out.println(y);
	}
	void fun2(){
		System.out.println(x);
		System.out.println(y);		// static variable axes from non static method 
	}
}
class Client1{			
	public static void main(String[]args){
		Demo.fun1();			// direct axes static variable
		Demo obj = new Demo();
		obj.fun2();
		System.out.println(obj.y);
	}
}
